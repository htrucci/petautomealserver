package com.htrucci.petautomealserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PetautomealserverApplication {

	public static void main(String[] args) {
		SpringApplication.run(PetautomealserverApplication.class, args);
	}
}
