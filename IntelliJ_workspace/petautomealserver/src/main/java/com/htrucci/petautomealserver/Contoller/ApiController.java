package com.htrucci.petautomealserver.Contoller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/petautomealserver")
@RestController
public class ApiController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());


    public static int status = 0;
    @RequestMapping(value = "test", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public String test(){
        logger.info("Test");
        //System.out.println("Test");
        return "Test";
    }
    @RequestMapping(value = "open", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public String open(){
        logger.info("Open");
        //System.out.println("Open");
        status = 1;
        return "1";
    }
    @RequestMapping(value = "getstatus", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public String getstatus(){
        logger.info("getStatus"+status);
        //System.out.println("getStatus"+status);
        return String.valueOf(status);
    }
    @RequestMapping(value = "close", method = RequestMethod.GET)
    @ResponseStatus(value = HttpStatus.OK)
    public String close(){
        logger.info("close");
        //System.out.println("close");
        status = 0;
        return String.valueOf(status);
    }
}
